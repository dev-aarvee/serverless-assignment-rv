'use strict';

module.exports.hello = async (event, context) => {

  var message = 'Hello World';
  const name = event.queryStringParameters && event.queryStringParameters.name

  if(name !== null)
  {
    message = 'Hello ' + name;
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: message,
      input: event,
    }),
  };
};

module.exports.moi = async (event, context) => {

  var message = 'Hello World';
  const name = event.pathParameters && event.pathParameters.name;

  if(name !== null)
  {
    message = 'Hello ' + name;
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: message,
      input: event,
    }),
  };
};